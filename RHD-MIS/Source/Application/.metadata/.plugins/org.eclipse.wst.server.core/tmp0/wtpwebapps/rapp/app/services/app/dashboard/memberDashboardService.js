
'use strict';

define(['app', 'services/utils/configurationService'], function (app) {

    var memberDashboardService = function ($resource, $q, configurationService, constantService) {
    debugger;	
    	var memberDashboardResource, delay;
        
    	memberDashboardResource = $resource(configurationService.allDashboardInfo, {}, {
    		getMemberDashboardInfo: { method: 'POST'}
        });
        
        this.getMemberDashboardInfo = function (obj) {
            delay = $q.defer();
            memberDashboardResource.getMemberDashboardInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
  
    };
    
    app.service('memberDashboardService', ['$resource', '$q', 'configurationService', 'constantService', memberDashboardService]);
});







