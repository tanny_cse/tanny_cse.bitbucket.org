
'use strict';

define(['app', 'services/utils/configurationService'], function (app) {

    var saDashboardService = function ($resource, $q, configurationService, constantService) {
    	
    	var saDashboardResource, delay;
        
    	saDashboardResource = $resource(configurationService.allDashboardInfo, {}, {
    		getSADashboardInfo: { method: 'POST'}
        });
        
        this.getSADashboardInfo = function (obj) {
            delay = $q.defer();
            saDashboardResource.getSADashboardInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };        
    };
    
    app.service('saDashboardService', ['$resource', '$q', 'configurationService', 'constantService', saDashboardService]);
});







