'use strict';

define(['app'], function (app) {
    
	 var dppEntryController = function ($rootScope, $scope, $filter, $location, $route, messageService, constantService, navigationService,
			 localStorageService, ngTableParams, alertService, dppService, informationService, configurationService, 
			 ngProgress, modalService, authorizationService) {
		 
		 var userInfo, promis, modalDefaults = {  }, modalOptions = { };
			$scope.dpps = [];
			 
			var dppInfo = {};
		 
		      $scope.saveDpp = function(dppInfo){
		    	  
			 debugger;
			 dppInfo.operation = constantService.Save; // Get all dpp
			 dppInfo.loginBean = authorizationService.getUserInfo();
			 promis = dppService.saveDppInfoData(dppInfo);
				 debugger;
				promis.then(function(data) {
					debugger;
					 if (!data.success) {
						 	messageService.showMessage(constantService.Danger, 'Unable to load Dpp');
						 	return;
				     }
					 
					 modalDefaults = { 	templateUrl: 'app/partials/information.html' };
				    	modalOptions = 	{ 	headerText: 'New DPP', 
				    						bodyText: 'DPP saved successfully...',
				    						okButtonText: 'Ok'
				    					};
						informationService.showModal(modalDefaults, modalOptions).then(function (result) {
			  	        });
						$route.reload();
			});
				
		 };
		       var dppApproval = {};
		 
		 		$scope.submitDpp = function(dppApproval){
		 			debugger;
		 			
		 			dppApproval.operation = constantService.Submit; // Get all dppapproval
		 			dppApproval.loginBean = authorizationService.getUserInfo();
					 promis = dppService.submitDppData(dppApproval);
					 
					 debugger;
						promis.then(function(data) {
							debugger;
							 if (!data.success) {
								 	messageService.showMessage(constantService.Danger, 'Unable to load Dpp');
								 	return;
							 }
							 modalDefaults = { 	templateUrl: 'app/partials/information.html' };
						    	modalOptions = 	{ 	headerText: 'New DPP', 
						    						bodyText: 'DPP submitted successfully...',
						    						okButtonText: 'Ok'
						    					};
								informationService.showModal(modalDefaults, modalOptions).then(function (result) {
					  	        });
								$route.reload();
					});
		 };
		 
		 
	
			 var init = function () {
			 		ngProgress.start();
				    ngProgress.complete();
			 	};

			 	init();
	 	
	 };
	 
    app.register.controller('dppEntryController', ['$rootScope', '$scope', '$filter', '$location', '$route',
	'messageService', 'constantService', 'navigationService', 'localStorageService', 'ngTableParams', 'alertService', 'dppService', 'informationService',
	'configurationService', 'ngProgress', 'modalService', 'authorizationService',dppEntryController]);
   
	
});

