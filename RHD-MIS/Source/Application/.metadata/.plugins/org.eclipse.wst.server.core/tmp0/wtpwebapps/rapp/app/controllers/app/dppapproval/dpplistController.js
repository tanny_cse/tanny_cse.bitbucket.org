
'use strict';

define(['app'], function (app) {
    
	 var dpplistController = function ($rootScope, $scope, $modal, _, $filter, messageService, dppListService, ngTableParams,
	     constantService, navigationService, localStorageService, configurationService, ngProgress,loadService,authorizationService ) {
		 
		 var userInfo, dppInfo, promis;
		    
	       
	        $scope.pageSize = 10;
	        $scope.itemsPerPage = 10;
	        $scope.currentPage = 1;
	        
	        $scope.dppInfo = { };
	        
	        $scope.pageDataBegin = 0;
	        $scope.pageDataEnd = 0;
	        $scope.pageDataTotal = 0;
	        $scope.pageItemText = ""; 
	        
	        $scope.dppInfos= [];
	        $scope.filteredDppInfos = [];
	        $scope.filteredTotalRecords = 0;
	     	        
	        $scope.goEditPage = function (dppInfo) {	        
	        	navigationService.showPageWithData('newdppentry', dppInfo.oid);
	        };
	      
	        $scope.goDPPEntryPage =function (){
	        	navigationService.menuNavigation('newdppentry');
	        };
	        
	        var filterDppInfos = function (filterText) {
	        	debugger;
	        	$scope.filteredDppInfos = $filter("dppInfoFilter")($scope.dppInfo, filterText);
	        	$scope.filteredTotalRecords =  Math.ceil($scope.filteredDppInfos.length);
	        	
	        	$scope.pageDataTotal = $scope.filteredTotalRecords;
	        	if($scope.pageDataTotal == 0){
	        		$scope.pageDataBegin = 0;
	            	$scope.pageDataEnd = 0;        		    		
	    		} else {
	        		$scope.pageDataBegin = (($scope.currentPage - 1) * $scope.pageSize) + 1;
	            	$scope.pageDataEnd = $scope.pageDataBegin + $scope.pageSize - 1;    		
	    		}
	        	
	        	if($scope.pageDataTotal != 0 && $scope.pageDataEnd > $scope.pageDataTotal) {
	        		$scope.pageDataEnd = $scope.pageDataTotal
	        	}  
	        	       	
	    		$scope.pageItemText = constantService.getPageItemText($scope.pageDataBegin, $scope.pageDataEnd, 
						$scope.pageDataTotal, "Users", 'English');
	        };

	        $scope.numPages = function () {
	        	return Math.ceil($scope.dppInfos.length / $scope.pageSize);
	        };
	    	
	    	var createWatches = function () {
	        	$scope.$watch("searchText", function (filterText) {
	        		filterUsers(filterText);
	            	$scope.currentPage = 1;
	            });
	            
	            $scope.$watch('currentPage + pageSize', function() {
	            	var begin = (($scope.currentPage - 1) * $scope.pageSize), end = begin + ($scope.pageSize - 0);
	            	$scope.filteredDppInfos = $scope.dppInfos.slice(begin, end);
	            	$scope.pageDataTotal = $scope.filteredTotalRecords;
	            	if($scope.pageDataTotal == 0) {
	            		$scope.pageDataBegin = 0;
	                	$scope.pageDataEnd = 0;        		    		
	        		} else {
	            		$scope.pageDataBegin = begin + 1;
	                	$scope.pageDataEnd = end;
	        		}
	            	if($scope.pageDataTotal != 0 && $scope.pageDataEnd > $scope.pageDataTotal) {
	            		$scope.pageDataEnd = $scope.pageDataTotal
	            	}
	        		$scope.pageItemText = constantService.getPageItemText($scope.pageDataBegin, $scope.pageDataEnd, 
							$scope.pageDataTotal, "Dpp Info", "English");
	            });
	        };
	        
	        
	        var getDppInfolist = function() {
				var dppInfoObj = {
					operation : constantService.GetAll,
					loginBean : userInfo
				};
				debugger;
				promis = dppListService.getDppInfolist(dppInfoObj);
				promis.then(function(data) {					
					debugger;
					if (!data.success) {
						messageService.showMessage(constantService.Danger,'Unable to load Dpp');
						return;
					}					
					$scope.dppInfos = data.data;
					console.log($scope.dppInfo);
					filterUsers('');
	        		createWatches();
				});
			};
			  
			
	 	var init = function () {
	 		ngProgress.start(); 
	 		getDppInfolist();	 		
	 		ngProgress.complete();
	 	};

	 	init();
	 	
	 };
	 
	 
    app.register.controller('dpplistController', ['$rootScope', '$scope', '$filter','messageService', 
     'dppListService', 'ngTableParams','constantService', 'navigationService', 'localStorageService','configurationService',
     'ngProgress', 'loadService', 'authorizationService', dpplistController]);
   
	
});

