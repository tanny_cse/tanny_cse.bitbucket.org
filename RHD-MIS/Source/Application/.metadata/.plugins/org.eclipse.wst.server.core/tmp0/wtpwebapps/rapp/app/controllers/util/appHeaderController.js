﻿
'use strict';

define(['app'], function (app) {
    
	 var appHeaderController = function ($rootScope, $scope, $log, $window, localStorageService, 
			 constantService, navigationService, $http, dataService, $location, signInService) {
		 
		 var userInfo, promis;
	    	
		$scope.logout = function (logoutObj) {		
        	userInfo = localStorageService.getValue(constantService.userInfoCookieStoreKey);
        	userInfo.operation = constantService.logout;
            promis = signInService.postObject(userInfo);
            promis.then(function (data) {
                localStorageService.setValue(constantService.userInfoCookieStoreKey, null);
                $location.path('/');
                $location.replace();
            });
        };


        var init = function () {
			userInfo = localStorageService.getValue(constantService.userInfoCookieStoreKey);
        	$scope.loggedinUserInfo = userInfo;
			$scope.webLeftMenus = $.parseJSON(userInfo.menuJSON);
			$scope.logoutObj = $scope.webLeftMenus[$scope.webLeftMenus.length-1];
			$scope.webLeftMenus.pop();
	    }; 
	    
	    init();
       
		 
    };

    app.controller('appHeaderController', ['$rootScope', '$scope', '$log', '$window', 'localStorageService',
    'constantService', 'navigationService', '$http', 'dataService', '$location', 'signInService', appHeaderController]);
   
	
});














