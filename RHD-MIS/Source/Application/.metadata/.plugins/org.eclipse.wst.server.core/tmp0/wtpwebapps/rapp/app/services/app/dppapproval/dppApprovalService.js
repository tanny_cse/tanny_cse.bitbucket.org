'use strict';

define(['app'], function (app) {

	var dppApprovalService = function ($rootScope, $resource, $q, constantService, configurationService, messageService, authorizationService) {
		
		var dppApprovalResource, delay;
        
		dppApprovalResource = $resource(configurationService.DppApproval, {}, {
			submitDppData :{method :'POST'}
			
		});
        
        this.submitDppData = function (obj) {
            delay = $q.defer();
            dppApprovalResource.submitDppData(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
       	
       
    };
    
    app.service('dppApprovalService', ['$rootScope', '$resource', '$q', 'constantService', 'configurationService', 
    'messageService','authorizationService', dppApprovalService]);

});


