
'use strict';

define(['app'], function (app) {

    var configurationService = function ($rootScope) {
    	
    	this.server = 'localhost';
        this.port = ':8080';
        this.appname = '/rapp/';
        
        // Local Host
        this.apprest = this.appname+'rest/';
        
        // Cloud Foundry
    	//this.apprest = '/rest/';		
        
        this.rootUrl = 'http://' + this.server + this.port + this.appname;
        this.baseUrl = 'http://' + this.server + this.port + this.apprest;
    	this.wsBaseUrl = 'ws://' + this.server + this.port +this.appname;
		//this.fileupload = this.apprest + 'fileupload/upload';
		
		// Sing up/ member registration
		this.signup = this.apprest + 'signup/post';
		
		// forgot password and sign in
    	this.login = this.apprest + 'login/post';

    	// For change password, new user (save user and update user status) and user profile (update user profile)
		this.userSecurity = this.apprest + 'usersecurity/post';
		
		// Update member profile and update member status 
		this.memberac = this.apprest + 'memberac/post';
		
    	this.data = this.apprest + 'data/post';
		this.dtwtransaction = this.apprest + 'transaction/post';
		  
		this.tradeRequest = this.apprest + 'traderequest/post';  
		this.trade = this.apprest + 'trade/post';
		this.tradeDiscontinueRequest = this.apprest + 'tradediscontinuerequest/post';
		this.depositcompany = this.apprest + 'depositcompany/post';
		
		// Dashboard - For member dashboard, admin dashboard, CRO dashboard and SA dashboard
		this.allDashboardInfo = this.apprest + 'dashboardInfo/post';
		this.DppInformation = this.apprest + 'dppInfo/post';
		this.DppApproval = this.apprest + 'dppApp/post';
		this.DppInfoList = this.apprest + 'dppList/post';
		    	
    };
    
    app.service('configurationService', ['$rootScope', configurationService]);

});


