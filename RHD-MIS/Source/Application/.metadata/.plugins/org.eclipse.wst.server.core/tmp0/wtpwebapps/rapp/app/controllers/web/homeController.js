
'use strict';

define(['app'], function (app) {
    
    var homeController = function ($scope, $location, $filter, $log, constantService, 
		localStorageService, messageService) {
    	
    	var userInfo, promis;
    	
    	$scope.myInterval = 1000*50;
    	$scope.slides = [
    	    {
    	      image: 'images/slides/1.jpg'
    	    },
    	    {
    	      image: 'images/slides/2.jpg'
    	    },
    	    {
    	      image: 'images/slides/3.jpg'
    	    },
    	    {
    	      image: 'images/slides/4.jpg'
    	    },
    	    {
      	      image: 'images/slides/5.jpg'
      	    },
    	    {
      	      image: 'images/slides/6.jpg'
      	    },
    	    {
    	      image: 'images/slides/7.jpg'
    	    }
    	];

        var init = function () {
        	
         };
        
        init();
        
    };
    
    
    
    app.register.controller('homeController', ['$scope', '$location', '$filter', '$log',
           'constantService', 'localStorageService', 'messageService',
           homeController]);

});


