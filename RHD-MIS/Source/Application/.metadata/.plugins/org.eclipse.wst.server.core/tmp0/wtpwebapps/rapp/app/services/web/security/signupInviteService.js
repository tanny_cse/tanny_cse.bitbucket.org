﻿
'use strict';

define(['app'], function (app) {

	var signupInviteService = function ($rootScope, $resource, $q, configurationService) {
		
		var signupInviteResource, delay, postObject;
	    
		signupInviteResource = $resource(configurationService.signUpInvite, {}, {
			postObject: { method: 'POST' }
		});
	    
		
		this.postObject = function (obj) {
            delay = $q.defer();
            signupInviteResource.postObject(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
        
        this.isBlank = function(){
        	var b = new Boolean(true);
        	var emailIdOfInvitee = $('#txtEmailAddressOfInvitee').val();
        	if(emailIdOfInvitee === '' ||emailIdOfInvitee === null || emailIdOfInvitee === undefined ){
        		$("#validaTionEmailAddressOfInvitee").show();
        		b = false;
        	}else{
        		$("#validaTionEmailAddressOfInvitee").hide();
        	}
        	return b;
        };
    };
    
    app.service('signupInviteService', ['$rootScope', '$resource', '$q', 'configurationService', signupInviteService]);

});

