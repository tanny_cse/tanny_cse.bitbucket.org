
'use strict';

define(['app'], function (app) {

    var constantService = function ($rootScope, $cookieStore) {

        this.getAppLayout = function () {
            var layout = {
                header : { location: 'app/views/layout/app/Header.html', isVisible: true },
                topMenu : { location: 'app/views/layout/app/TopMenu.html', isVisible: true },
                footer : { location: 'app/views/layout/app/Footer.html', isVisible: true }
            };
            return layout;
        };

        this.getWebLayout = function () {
            var layout = {
                header : { location: 'app/views/layout/web/Header.html', isVisible: true },
                topMenu : { location: 'app/views/layout/web/TopMenu.html', isVisible: true },
                footer : { location: 'app/views/layout/web/Footer.html', isVisible: true }
            };
            return layout;
        };
        
        this.getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        
        this.OwnerName = 'Oppo Finance UK Limited';
        this.Url = 'http://localhost:8080/oppofinance/#/signup/';

              
        // Uploaded Document Path // Query String in URL // Document upload, download // A/c verification
        this.DocumentUpload 		= 'o0_werus8_9y5l_o_fj__acvx___b9pi94_3f_sj68__9tfgd9v929lo____92nn748d_vc9a_________s_dos_dfug';//DocumentUpload
        this.URLMemberLoginOID 		= 'w7qa__x3ac_2hgfh_13z__45of_drw7e_______7jmk_d67s_475j_sovfb_qt__________ry_m56_kfad';
        this.NavigationOperation	= 'yq0ax6__45rjgj____hzjt_c_23gf_4fhz___hm4________erw__e___5okf_rw_ex6mn_tr8475j_sof_m56_kfad';
        this.DocumentName	 		= 't3qa8xc4_2__3wm45h4_fg5of_rwag7___6emr50______6e____________rjh_n84705j_s0_orfm56_kfad';
        this.DocumentType 			= 'evm_i94nc__u8n5na62n3t_7x_hfl_04_______n3844nax0__________1c9g7q__idkrbsu_________cvfhrqy486kf8j';
    	this.QueryStringMemberAcOid	= "qs1c2v_bn_________4m5ko________6uyhgvf7etf_________k_________a6a08z_xcbm3__0kl0y9_____uiesf8t___";
    	this.QueryStringUploadedDocumentOid = "___uqhsf1vcb2m__vk_ibonp__5e__d6_s0__9g9_terd7h_u48m9o3__h0__6v7f7etf_v_h__j__ter56__er5f";
        this.FileNameLength = 32;
        this.FileSize = 524288;
        this.DocumentTypePIP = 'Personal Identity Proof';
        this.DocumentTypeAIP = 'Address Identity Proof';
        this.GetUploadedDocument = 'GetUploadedDocument';
        this.DocumentUploadInProgress = 'Document Upload In-Progress';

        // Paging
        this.pageSize = 20;
        this.pagesNumber = 20;
        
        // User Role
        this.SuperAdmin='SA_OID';
        this.Admin='Admin_OID';
        this.CustomerRelationshipOfficer='CRO_OID';
        this.Member='Member_OID';
        this.RoleSADetail='Super Admin';
        this.RoleAdminDetail='Admin';
        this.RoleCRODetail = 'Customer Relationship Officer';
        this.RoleMemberDetail='Member';

        // Report related /////////////////////////////////////////////////////////////////////////////
        this.ReportDeposit = 'ReportDeposit';
        this.ReportTransferSend = 'ReportTransferSend';
        this.ReportTransferReceive = 'ReportTransferReceive';
        this.ReportWithdrawal = 'ReportWithdrawal';
        this.ReportDetailTradeAndTradingReward = 'ReportDetailTradeAndTradingReward';
        this.ReportTradeAndTradingRewardHistory = 'ReportTradeAndTradingRewardHistory';
        this.ReportTradeAndTradingRewardSummary = 'ReportTradeAndTradingRewardSummary';
        this.ReportTradeCommission = 'ReportTradeCommission';
        this.ReportCurrentBalance = 'ReportCurrentBalance';
        this.ReportTrailBalance = 'ReportTrailBalance';
        this.ReportAccountStatement = 'ReportAccountStatement';
        this.ReportIncomeStatement = 'ReportIncomeStatement';
        this.ReportMemberList = 'ReportMemberList';
        this.ReportRankWiseMember = 'ReportRankWiseMember';
        this.ReportTwtFunctionScheduleOff = 'ReportTwtFunctionScheduleOff';
        
        // Dashboard related /////////////////////////////////////////////////////////////////////////////
        this.GetSADashboardInfo = 'GetSADashboardInfo';
        this.GetAdminDashboardInfo = 'GetAdminDashboardInfo';
        this.GetCRODashboardInfo = 'GetCRODashboardInfo';
        this.GetMemberDashboardInfo = 'GetMemberDashboardInfo';        		        

        this.userInfoCookieStoreKey = 'user.info.cookie.store.key';
        this.AlertMessage = 'AlertMessage';
        
        this.Login = 'Login';
        this.Logout = 'Logout';
        
        this.Active = 'Active';
        this.Inactive = 'Inactive';
        this.Lock = 'Lock';
        this.Block = 'Block';
        this.Terminate = 'Terminate';
        
        this.Yes = 'Yes';
        this.No = 'No';
        this.Ok = 'Ok';
        this.All = 'All';
        
        this.Save = 'Save';
        this.Update = 'Update';
        this.Submit = 'Submit';        
        this.Delete = 'Delete';
        this.Verify = 'Verify';
        this.Approve = 'Approve';
        this.Cancel = 'Cancel';
        this.GetAll = 'GetAll';
        this.UpdateProfile = 'UpdateProfile';
        this.UpdateMemberUserStatus = 'UpdateMemberUserStatus';
        this.GetAllRole = 'GetAllRole';
        this.GetAllUser = 'GetAllUser';
        this.GetByLOginID = 'GetByLOginID';
        this.GetDataByOid = 'GetDataByOid';
        this.GetUserByLoginID = 'GetUserByLoginID';
        this.GetAllDtw = 'GetAllDtw';
        this.SaveJournal = 'SaveJournal';
        this.UpdateJournal = 'UpdateJournal';
        this.DeleteJournal = 'DeleteJournal';
        this.GetJournalDataByOid = 'GetJournalDataByOid';
        
        this.SaveReport = 'SaveReport';
        this.UpdateReport = 'UpdateReport';
        this.DeleteReport = 'DeleteReport';
        this.GetReportDataByOid = 'GetReportDataByOid';
        this.CheckPin = 'CheckPin';
        this.ResendPin = 'ResendPin';
        this.GetAllCommission = 'GetAllCommission';
        this.UpdateCommissionRate = 'UpdateCommissionRate';
        this.GetByEmailID = 'GetByEmailID';
        this.DeleteByID = 'DeleteByID';
        this.SaveAcVerification = 'SaveAcVerification';
        this.SaveRegistrationData ='SaveRegistrationData';

        this.Pending = 'Pending';
        this.PendingForApproval = 'Pending For Approval';
        this.DepositPendingForApproval = 'Deposit Pending For Approval';
        this.TransferPendingForApproval = 'Transfer Pending For Approval';
        this.WithdrawalPendingForApproval = 'Withdrawal Pending For Approval';
        this.Approved ='Approved';
        this.Cancelled = 'Cancelled';
        this.Verified = 'Verified';
        this.Unverified = 'Unverified';
        this.Deposit = 'Deposit';
        this.Transfer = 'Transfer';
        this.Withdrawal = 'Withdrawal';
        this.ApproveCancel = 'ApproveCancel';
        this.FunctionOffOn = 'FunctionOffOn';
        this.RequestHigherRank = 'RequestHigherRank';
        this.ForgotPassword = 'ForgotPassword';
        this.ChangePassword = 'ChangePassword';
        this.ResetPassword = 'ResetPassword';

        this.Danger = 'danger';
        this.Success = 'success';
        this.Info = 'info';
        this.Warning = 'warning';

        
        this.ReportMemberBalance='ReportMemberBalance';
        
        this.getPageItemText = function(pageDataBegin, pageDataEnd, pageDataTotal, recordTypeText, language) {
        	var pageItemText = "Showing "+pageDataBegin+
			" to "+pageDataEnd+
			" of "+pageDataTotal+
			" total "+recordTypeText+".";
			
			return pageItemText;       	
        };
        
        this.getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
    };
    
    app.service('constantService', ['$rootScope', '$cookieStore',  constantService]);

});
