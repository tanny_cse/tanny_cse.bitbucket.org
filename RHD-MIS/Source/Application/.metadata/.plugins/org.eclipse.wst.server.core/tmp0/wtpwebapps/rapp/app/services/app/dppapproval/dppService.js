
'use strict';

define(['app'], function (app) {

	var dppService = function ($rootScope, $resource, $q, constantService, configurationService, messageService, authorizationService) {
		
		var dppResource, delay, dppApprovalResource;
		
		dppApprovalResource = $resource(configurationService.DppApproval, {}, {
			submitDppData :{method :'POST'}
			
		});
	    
		dppResource = $resource(configurationService.DppInformation, {}, {
			getDpps: { method: 'POST' },
			saveDppData : {method : 'POST'}
			
		});
        
		this.submitDppData = function (obj) {
            delay = $q.defer();
            dppApprovalResource.submitDppData(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };

		
        this.getDpps = function (obj) {
            delay = $q.defer();
            dppResource.getDpps(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
        
        this.saveDppInfoData = function (obj) {
            delay = $q.defer();
            dppResource.saveDppData(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
        
  
    };
    
     
    
    app.service('dppService', ['$rootScope', '$resource', '$q', 'constantService', 'configurationService', 
    'messageService','authorizationService', dppService]);

});


