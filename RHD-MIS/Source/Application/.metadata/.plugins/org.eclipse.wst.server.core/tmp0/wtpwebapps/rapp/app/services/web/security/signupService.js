﻿
'use strict';

define(['app'], function (app) {

	var signupService = function ($rootScope, $resource, $q, configurationService) {
		
		var signupResource, delay, postObject;
	    
		signupResource = $resource(configurationService.signup, {}, {
			postObject: 				{ method: 'POST' },
			getPinInformation: 			{ method: 'POST'},
			postRegistrationObject: 	{ method: 'POST'},
			resendPinInfo: 				{ method: 'POST'}			
		});
	    
		
		this.postObject = function (obj) {
            delay = $q.defer();
            signupResource.postObject(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };	
        
        this.postRegistrationObject = function (obj) {
            delay = $q.defer();
            signupResource.postRegistrationObject(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };	
      
        this.getPinInformation = function (obj) {
            delay = $q.defer();
            signupResource.getPinInformation(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
        
        this.resendPinInfo = function (obj) {
            delay = $q.defer();
            signupResource.resendPinInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };       
	};
    
    app.service('signupService', ['$rootScope', '$resource', '$q', 'configurationService', signupService]);

});

