﻿
'use strict';

define(['services/utils/routeResolver'], function () {

	var app = angular.module('mbmsApp', ['localization', 'ngRoute', 'ngAnimate', 'ngResource',  
		  'ngCookies', 'ui.bootstrap', 'ui', 'ui.select2', 'highcharts-ng', 'ngTable', /*'reCAPTCHA',*/
		  'routeResolverServices', 'underscore', 'ui.bootstrap.transition', 'ngProgress', 'angularFileUpload']);

	app.run(['$rootScope', '$route', '$http', '$location', 'constantService', 'localize', 'authorizationService',
	         function ($rootScope, $route, $http, $location, constantService, localize, authorizationService) {
	
		var userInfo;
		$rootScope.messagePageLocation = 'app/partials/message.html';
		localize.setLanguage('en-US');
		
		$rootScope.$on("$routeChangeStart", function (oldPath, newPath) {
			$rootScope.isWeb = true;
			if (newPath.$$route == undefined || newPath.$$route.isWeb) {
	        	$rootScope.layout = constantService.getWebLayout();
	            return;
	        } 
	        userInfo = authorizationService.getUserInfo();
	        if(userInfo === undefined || userInfo === null){
	            $rootScope.layout = constantService.getWebLayout();
	            $location.path('/');
	            return;
	        }
	        //$rootScope.isWeb = false;
	        $rootScope.layout = constantService.getAppLayout();
	    });
    
	}]); 

	app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', 
	            '$filterProvider', '$provide', '$locationProvider', '$httpProvider', /*'reCAPTCHAProvider',*/
	         function ($routeProvider, routeResolverProvider, $controllerProvider, $compileProvider, 
	        	$filterProvider, $provide, $locationProvider, $httpProvider/*, reCAPTCHAProvider*/) {
		
		// required, please use your own key :)
        //reCAPTCHAProvider.setPublicKey('6Lc7rQoTAAAAAOBgmm2eYukI_hHDYZxrdycsY6sP');
        // optional
        /*reCAPTCHAProvider.setOptions({
            theme: 'clean'
        });*/
		
		app.register = {
	        controller: $controllerProvider.register,
	        //directive: $compileProvider.directive,
	        filter: $filterProvider.register,
	        //factory: $provide.factory,
	        //service: $provide.service
	    };
		
		// Provider-based service.
        app.service = function( name, constructor ) {
            $provide.service( name, constructor );
            return( this );
        };
        
        // Provider-based factory.
        app.factory = function( name, factory ) {
            $provide.factory( name, factory );
            return( this );
        };
        
        // Provider-based directive.
        app.directive = function( name, factory ) {
            $compileProvider.directive( name, factory );
            return( this );
        };
     
		var route = routeResolverProvider.route;
		$routeProvider
		//page and controller name prefix, dir path, title, isWeb
		.when('/', 									route.resolve('home', 							'web/',					'Home', 								true))
		.when('/home', 								route.resolve('home', 							'web/',					'Home', 								true))
		.when('/signin', 							route.resolve('signin', 						'web/security/',		'Sign in', 								true))
		.when('/sadashboard', 						route.resolve('saDashboard', 					'app/dashboard/',		'Dashboard', 							false))
		.when('/admindashboard', 					route.resolve('adminDashboard', 				'app/dashboard/',		'Dashboard', 							false))
		.when('/userdashboard', 					route.resolve('userDashboard', 					'app/dashboard/',		'Dashboard', 							false))
		.when('/systemadmin', 						route.resolve('systemAdmin', 					'app/',					'System Admin', 						false))
		.when('/security', 							route.resolve('security', 						'app/',					'Security', 							false))
		.when('/newdppentry',						route.resolve('dppEntry', 		                'app/dppapproval/',     'DPP Entry', 	                        false))
		.when('/dppinfolist', 						route.resolve('dpplist', 				        'app/dppapproval/',     'List Of dppInfo',                          false))
		.when('/changepassword', 					route.resolve('changePassword', 				'app/systemsetting/',	'Change Password', 						false))
		.when('/userprofile', 						route.resolve('updateUserProfile', 				'app/systemsetting/',	'User Profile', 						false))
		.otherwise({ redirectTo: '/' });
		
	}]);
	
	return app;

});



    
    
    
    
    