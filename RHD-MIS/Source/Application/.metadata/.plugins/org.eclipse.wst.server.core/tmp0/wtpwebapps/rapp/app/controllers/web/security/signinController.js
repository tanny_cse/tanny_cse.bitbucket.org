﻿
'use strict';

define(['app'], function (app) {
	
	var signinController = function ($rootScope, $scope,  signInService, localStorageService, configurationService, constantService, 
		ngProgress, navigationService, authorizationService, loadService, messageService, inputValidationService, informationService) {

	    var promis;
		$scope.login = { loginID : 'sarwar', userPassword : '1234', msg : 'Df1000' };
		var modalDefaults = { }, modalOptions = { };
		
///////////////////////////////////////////// Login Panel - Validate user credential from server //////////////////////////////////		
		$scope.signIn = function (login) {
		debugger;	
			if(!inputValidationService.isBlankLoginID('alert_heading_login', 'alert_body_login_id_empty')){
				return;
			}
			
			if(!inputValidationService.isBlankPassword('alert_heading_login', 'alert_body_password_empty')){
				return;
			}
			
	//		navigationService.menuNavigation('/app/dashboard/adminDashboard.html');
			
        	var userObj = {loginID : login.loginID, userPassword : login.userPassword, operation : constantService.Login};
			promis = signInService.postObject(userObj);
			promis.then(function (data) {
				debugger;	
				if (!data.success) {
					messageService.showMessage(constantService.Danger, data.code);
					return;
				}
				debugger;
				var menuJson = $.parseJSON(data.data.menuJSON);
				var userInfo = data.data;
				if(menuJson[0].child == undefined || menuJson[0].child == null || menuJson[0].child.length == 0){
					userInfo.selectedLeftMenu = menuJson[0].url;
				} else {
					userInfo.selectedLeftMenu = menuJson[0].child[0].url;
				}
			debugger;	
				localStorageService.setValue(constantService.userInfoCookieStoreKey, userInfo);
				navigationService.menuNavigation(userInfo.selectedLeftMenu);

			});
		};
		
///////////////////////////////////////////// Login Panel - Navigate to Sign In page ////////////////////////////////////////////////
		$scope.goToSignUpPage = function(){
			navigationService.menuNavigation('signup');
		};

		
		
/////////////////////////////////////////// Forgot Password Panel - Fill up Security Question DDL /////////////////////////////////////////////   	
		$scope.securityQuestions = [
		   						  {"key": constantService.SecurityQuestionOne, "value": constantService.SecurityQuestionOne },
		   						  {"key": constantService.SecurityQuestionTwo, "value": constantService.SecurityQuestionTwo},
		   						  {"key": constantService.SecurityQuestionThree, "value": constantService.SecurityQuestionThree},
		   						  {"key": constantService.SecurityQuestionFour, "value": constantService.SecurityQuestionFour},
		   						  {"key": constantService.SecurityQuestionFive, "value": constantService.SecurityQuestionFive},
		   						  {"key": constantService.SecurityQuestionSix, "value": constantService.SecurityQuestionSix},
		   						  {"key": constantService.SecurityQuestionSeven, "value": constantService.SecurityQuestionSeven},
		   						  {"key": constantService.SecurityQuestionEight, "value": constantService.SecurityQuestionEight}
		   				         ];

///////////////////////////////////////////// Forgot Password Panel - Check blank Email Address //////////////////////////////////
		$scope.checkEmailAddress = function(email) {
        	inputValidationService.checkEmailAddress(email);
        };
        
///////////////////////////////////////////// Forgot Password Panel - Check blank Security Question Answer //////////////////////////////////
    	$scope.checkSecurityQuestionAnswer = function(sqa) {
        	inputValidationService.checkSecurityQuestionAnswer(sqa);
        };
        
///////////////////////////////////////////// Forgot Password Panel - Send password by mail to user //////////////////////////////////////    	
    	$scope.sendPasswordToEmail = function (userInfoData) {
    		
    		if (!inputValidationService.isBlankEmailAddress('alert_heading_forgot_password', 'alert_body_email_address_empty')) {    			
         		return; 
         	}
    		if (!inputValidationService.isBlankSecurityQuestion('alert_heading_forgot_password', 'alert_body_security_question_empty')) {    			
         		return; 
         	}
        	if (!inputValidationService.isBlankSecurityQuestionAnswer('alert_heading_forgot_password', 'alert_body_security_question_answer_empty')) {    			
         		return;
         	}
    		
        	ngProgress.start();
			loadService.showDialog();
			
    		userInfoData.operation = constantService.ForgotPassword;
        	promis = signInService.forgotPassword(userInfoData);
            promis.then(function (data) {
            	loadService.hideDialog();
				ngProgress.complete();
            	if (!data.success) {
            		messageService.showMessage(constantService.Danger, data.code);
                    return;
                }
            	//messageService.showMessage(constantService.Success, data.code);
				modalDefaults = { templateUrl: 'app/partials/information.html' };
		    	modalOptions = { headerText: 'Forgot Password', bodyText: 'Password recovered, check your email...' };
				informationService.showModal(modalDefaults, modalOptions).then(function (result) {
					navigationService.menuNavigation('home');
	  	        });				
            });
		};
        
	 	var init = function () {
	 		
	 	};

	 	init();
		 
 	};
 	
    app.register.controller('signinController', ['$rootScope', '$scope', 'signInService', 'localStorageService', 'configurationService', 'constantService', 
    'ngProgress', 'navigationService', 'authorizationService', 'loadService', 'messageService', 'inputValidationService', 'informationService', signinController]);
	
});














