
'use strict';

define(['app'], function (app) {

    var dppInfoFilter = function () {

        return function (dppInfos, filterValue) {
            if (!filterValue) return dppInfos;
            var matches = [];
            filterValue = filterValue.toLowerCase();
            for (var i = 0; i < dppInfos.length; i++) {
                var dppInfo = dppInfos[i];
                if ((dppInfo.projectName != undefined && dppInfo.projectName.toLowerCase().indexOf(filterValue) > -1) ||
                	(dppInfo.oId != undefined && dppInfo.oId.toLowerCase().indexOf(filterValue) > -1)) {
                    matches.push(dppInfo);
                }
            }
            return matches;
        };
    };

    app.filter('dppInfoFilter', dppInfoFilter);

});