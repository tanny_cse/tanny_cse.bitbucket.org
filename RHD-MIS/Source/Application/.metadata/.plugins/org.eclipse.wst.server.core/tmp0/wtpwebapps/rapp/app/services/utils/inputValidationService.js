
'use strict';

define(['app'], function (app) {

    var inputValidationService = function (alertService) {

		var modalDefaults = { templateUrl: 'app/partials/alert.html' };
		var modalOptions = { headerText: '', bodyText: '', okButtonText: 'Ok' };

		this.checkFirstName = function(fn) {
        	if(fn == '' || fn == undefined) {
    			$("#validaTionFirstName").show();    			
    		} else {
    			$("#validaTionFirstName").hide();
    		}
        };
        
        this.checkEmailAddress = function(ea) {
        	if(ea == '' || ea == undefined) {
    			$("#validaTionEmailAddress").show();
    		} else {
    			$("#validaTionEmailAddress").hide();
    		}
        };
        
        this.checkMobileNumber = function(mn) {
        	if(mn == '' || mn == undefined) {
    			$("#validaTionMobileNumber").show();
    		} else {
    			$("#validaTionMobileNumber").hide();
    		}
        };
        
        this.checkPINCode = function(pc) {
        	if(pc == '' || pc == undefined) {
    			$("#validaTionPINCode").show();
    		} else {
    			$("#validaTionPINCode").hide();
    		}
        };
        
        this.checkSecurityQuestionAnswer = function(sqa) {
        	if(sqa == '' || sqa == undefined) {
    			$("#validaTionSecurityQuestionAnswer").show();
    		} else {
    			$("#validaTionSecurityQuestionAnswer").hide();
    		}
        };
        
        this.checkCommonTextInput = function(cti) {
        	if(cti == '' || cti == undefined) {
    			$("#validaTionCommonTextInput").show();
    		} else {
    			$("#validaTionCommonTextInput").hide();
    		}
        };
        
    	this.checkCommonTextInputWithId = function(inputData, objId) {
        	if(inputData == '' || inputData == undefined) {
        		$("#validaTionCommonTextInput"+objId).show();
    		} else {
    			$("#validaTionCommonTextInput"+objId).hide();
    		}
        };
        
        this.checkSecurityCode = function(sc) {
        	if(sc == '' || sc == undefined) {
    			$("#validaTionSecurityCode").show();
    		} else {
    			$("#validaTionSecurityCode").hide();
    		}
        };
        
        this.checkDateWhen = function(dw) {
        	if(dw == '' || dw == undefined) {
    			$("#validaTionDateWhen").show();
    		} else {
    			$("#validaTionDateWhen").hide();
    		}
        };
        
        this.checkFromDate = function(fd) {
        	if(fd == '' || fd == undefined) {
    			$("#validaTionFromDate").show();
    		} else {
    			$("#validaTionFromDate").hide();
    		}
        };
        
        this.checkToDate = function(td) {
        	if(td == '' || td == undefined) {
    			$("#validaTionToDate").show();
    		} else {
    			$("#validaTionToDate").hide();
    		}
        };
        
        this.commonCheckBlank = function(tValue, tField) {
        	if(tValue == '' || tValue == undefined) {
    			$('#'+tField).show();
    		} else {
    			$('#'+tField).hide();
    		}
        };
        
		this.isBlankFirstName = function(requiredField, msgBody) {  	
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var firstName = $('#txtFirstName').val();
			if (firstName == ''|| firstName == null || firstName.trim().length == 0) {
				$("#validaTionFirstName").focus();
				$("#validaTionFirstName").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });				
				return false;
			} else {
				$("#validaTionFirstName").hide();
			}
			return true;
		};
		 
		this.isBlankEmailAddress = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var email = $('#txtEmailAddress').val();
			if (email == ''|| email == null || email.trim().length == 0) {
				$("#validaTionEmailAddress").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionEmailAddress").hide();
			}
		 	return true;
		};
		
		this.isBlankCountry = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var c = $('#txtCountry').val();
			if (c == ''|| c == null || c.trim().length == 0) {
				$("#validaTionCountry").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionCountry").hide();
			}
		 	return true;
		};
		
		this.isBlankMobileNumber = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var mn = $('#txtMobileNumber').val();
			if (mn == ''|| mn == null || mn.trim().length == 0) {
				$("#validaTionMobileNumber").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionMobileNumber").hide();
			}
		 	return true;
		};
		
		this.isBlankPINCode = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var pc = $('#txtPINCode').val();
			if (pc == ''|| pc == null || pc.trim().length == 0) {
				$("#validaTionPINCode").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionPINCode").hide();
			}
		 	return true;
		};

		this.isBlankAccountType = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var at = $('#txtAccountType').val();
			if (at == ''|| at == null || at.trim().length == 0) {
				$("#validaTionAccountType").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionAccountType").hide();
			}
		 	return true;
		};
		
		
		this.isBlankGender = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var g = $('#txtGender').val();
			if (g == ''|| g == null || g.trim().length == 0) {
				$("#validaTionGender").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionGender").hide();
			}
		 	return true;
		};
		
		this.isBlankTotalEstimatedAnnualIncome = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var teai = $('#txtTotalEstimatedAnnualIncome').val();
			if (teai == ''|| teai == null || teai.trim().length == 0) {
				$("#validaTionTotalEstimatedAnnualIncome").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionTotalEstimatedAnnualIncome").hide();
			}
		 	return true;
		};
		
		this.isBlankTotalEstimatedNetWorth = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var tenw = $('#txtTotalEstimatedNetWorth').val();
			if (tenw == ''|| tenw == null || tenw.trim().length == 0) {
				$("#validaTionTotalEstimatedNetWorth").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionTotalEstimatedNetWorth").hide();
			}
		 	return true;
		};
		
		this.isBlankEmploymentStatus = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var es = $('#txtEmploymentStatus').val();
			if (es == ''|| es == null || es.trim().length == 0) {
				$("#validaTionEmploymentStatus").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionEmploymentStatus").hide();
			}
		 	return true;
		};
		
		this.isBlankSourceOfFund = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sof = $('#txtSourceOfFund').val();
			if (sof == ''|| sof == null || sof.trim().length == 0) {
				$("#validaTionSourceOfFund").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionSourceOfFund").hide();
			}
		 	return true;
		};		
		
		this.isBlankSecurityQuestion = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sq = $('#txtSecurityQuestion').val();
			if (sq == ''|| sq == null || sq.trim().length == 0) {
				$("#validaTionSecurityQuestion").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionSecurityQuestion").hide();
			}
		 	return true;
		};
		
		this.isBlankSecurityQuestionAnswer = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sqa = $('#txtSecurityQuestionAnswer').val();
			if (sqa == ''|| sqa == null || sqa.trim().length == 0) {
				$("#validaTionSecurityQuestionAnswer").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionSecurityQuestionAnswer").hide();
			}
		 	return true;
		};
		
		this.isBlankSecurityCode = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sc = $('#txtSecurityCode').val();
			if (sc == ''|| sc == null || sc.trim().length == 0) {
				$("#validaTionSecurityCode").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionSecurityCode").hide();
			}
		 	return true;
		};
		
		this.isBlankRole = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var role = $('#txtRole').val();
			if (role == ''|| role == null || role.trim().length == 0) {
				$("#validaTionRole").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionRole").hide();
		 	}
		 	return true;
		};
		
		this.isBlankTWTIndication = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var twtIndication = $('#txtTWTIndication').val();
			if (twtIndication == ''|| twtIndication == null || twtIndication.trim().length == 0) {
				$("#validaTionTWTIndication").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionTWTIndication").hide();
			}
		 	return true;
		};
		
		this.isBlankCommonTextInput = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var cti = $('#txtCommonTextInput').val();
			if (cti == ''|| cti == null || cti.trim().length == 0) {
				$("#validaTionCommonTextInput").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionCommonTextInput").hide();
			}
		 	return true;
		};

        this.isBlankCommonTextInputWithId = function(requiredField, msgBody, objId) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var cti = $('#txtCommonTextInput'+objId).val();
			if (cti == ''|| cti == null || cti.trim().length == 0) {
				$("#validaTionCommonTextInput"+objId).show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionCommonTextInput"+objId).hide();
			}
		 	return true;
		};
		
		this.isBlankLoginID = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var lId = $('#txtLoginId').val();
			if (lId == ''|| lId == null || lId.trim().length == 0) {
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				return true;
			}		 	
		};
		
		this.isBlankPassword = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var pwd = $('#txtPassword').val();
			if (pwd == ''|| pwd == null || pwd.trim().length == 0) {
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
			 	return true;
			}
		};
		
		this.isBlankOldPassword = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var cti = $('#oldPassword').val();
			if (cti == ''|| cti == null || cti.trim().length == 0) {
				$("#validaTionOldPassword").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionOldPassword").hide();
			}
		 	return true;
		};
		
		this.isBlankNewPassword = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var cti = $('#newPassword').val();
			if (cti == ''|| cti == null || cti.trim().length == 0) {
				$("#validaTionNewPassword").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionNewPassword").hide();
			}
		 	return true;
		};
		
		this.isBlankConfirmPassword = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var cti = $('#confirmPassword').val();
			if (cti == ''|| cti == null || cti.trim().length == 0) {
				$("#validaTionConfirmPassword").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionConfirmPassword").hide();
			}
		 	return true;
		}
		
	 	this.isBlankChooseFilePIP = function(requiredField, msgBody) {  	
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var ftuPip = $('#fileToUploadPIP').val();
			if (ftuPip == ''|| ftuPip == null || ftuPip.trim().length == 0) {
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			}
			return true;
		};
			
		this.isBlankChooseFileAIP = function(requiredField, msgBody) {  	
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var ftuAip = $('#fileToUploadAIP').val();
			if (ftuAip == ''|| ftuAip == null || ftuAip.trim().length == 0) {
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			}
			return true;
		}

		this.isBlankNewStatus = function(objMember, requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			if (objMember.newStatusOfMember == undefined || objMember.newStatusOfMember == "" || objMember.newStatusOfMember.trim().length == 0) {
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });
				return false;
			} else {
				return true;
			}
		};
		
		this.isCommonBlank = function(requiredField, msgBody, txtValue, txtField) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var commonText = $('#'+txtValue).val();
			if (commonText == ''|| commonText == null || commonText.trim().length == 0) {
				$('#'+txtField).show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$('#'+txtField).hide();
			}
		 	return true;
		}

		////////////////////////////////////////////////////////// Date and Time Related //////////////////////////////////////////////////		
		this.isBlankDateOfBirth = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var dob = $('#txtDateOfBirth').val();
			if (dob == ''|| dob == null || dob.trim().length == 0) {
				$("#validaTionDateOfBirth").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionDateOfBirth").hide();
			}
		 	return true;
		};
		
		this.isBlankDateWhen = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var dateWhen = $('#txtDateWhen').val();
			if (dateWhen == ''|| dateWhen == null || dateWhen.trim().length == 0) {
				$("#validaTionDateWhen").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionDateWhen").hide();
		 	}
		 	return true;
		};
		
		this.isBlankStartHour = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sh = $('#txtStartHour').val();
			if (sh == ''|| sh == null || sh.trim().length == 0) {
				$("#validaTionStartHour").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionStartHour").hide();
			}
		 	return true;
		};
		
		this.isBlankStartMinute = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var sm = $('#txtStartMinute').val();
			if (sm == ''|| sm == null || sm.trim().length == 0) {
				$("#validaTionStartMinute").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionStartMinute").hide();
			}
		 	return true;
		};
		
		this.isBlankEndHour = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var eh = $('#txtEndHour').val();
			if (eh == ''|| eh == null || eh.trim().length == 0) {
				$("#validaTionEndHour").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionEndHour").hide();
			}
		 	return true;
		};
		
		this.isBlankEndMinute = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var em = $('#txtEndMinute').val();
			if (em == ''|| em == null || em.trim().length == 0) {
				$("#validaTionEndMinute").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionEndMinute").hide();
			}
		 	return true;
		};		
		
		this.isBlankFromDate = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var tFD = $('#txtFromDate').val();
			if (tFD == ''|| tFD == null || tFD.trim().length == 0) {
				$("#validaTionFromDate").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionFromDate").hide();
			}
		 	return true;
		}
		
		this.isBlankToDate = function(requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
			var tTD = $('#txtToDate').val();
			if (tTD == ''|| tTD == null || tTD.trim().length == 0) {
				$("#validaTionToDate").show();
				alertService.showModal(modalDefaults, modalOptions).then(function (result) { });	  
				return false;
			} else {
				$("#validaTionToDate").hide();
			}
		 	return true;
		};
		
		this.compareFromToDateAsString = function(fromDate, toDate, requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
    		if (this.getNumberFromDate(fromDate) > this.getNumberFromDate(toDate)) {
    			alertService.showModal(modalDefaults, modalOptions).then(function (result) { });
    			return false;
    		} else {
    			return true;
    		}
		};
		
		this.compareFromToDateAsDate = function(fromDate, toDate, requiredField, msgBody) {
			modalOptions.headerText = requiredField;
			modalOptions.bodyText = msgBody;
    		if (fromDate.getTime() > toDate.getTime()) {
    			alertService.showModal(modalDefaults, modalOptions).then(function (result) { });
    			return false;
    		} else {
    			return true;
    		}
		};
		
		this.getNumberFromDate = function(dateAsString) {
			var dateComponent = dateAsString.split('/');
    		return parseInt(dateComponent[2]+''+dateComponent[1]+''+dateComponent[0]);
		};
		
		this.getDateAsString = function(dateAsDate) {
			var dateAsNumber = new Date(dateAsDate.getTime());
    		return ('0'+dateAsNumber.getDate()).slice(-2)+'/'+('0'+(dateAsNumber.getMonth()+1)).slice(-2)+'/'+dateAsNumber.getFullYear();
		};
		
    };
    
    app.service('inputValidationService', ['alertService',  inputValidationService]);
});
