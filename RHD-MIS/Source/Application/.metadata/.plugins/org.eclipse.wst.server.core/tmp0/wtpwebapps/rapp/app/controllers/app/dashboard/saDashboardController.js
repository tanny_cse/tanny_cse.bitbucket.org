
'use strict';

define(['app'], function (app) {
    
    var saDashboardController = function ($scope, $location, $filter, $log, constantService, authorizationService, saDashboardService) {
    	
    	var userInfo, promis;		
    	$scope.saDashboardInfo = [];
		
    	var getSADashboardInfo = function () {
        	var userObj = { operation : constantService.GetSADashboardInfo, loginBean : authorizationService.getUserInfo() };
        	promis = saDashboardService.getSADashboardInfo(userObj);
            promis.then(function (data) {
            	if (!data.success) {
                    return;
                }
            	$scope.saDashboardInfo = data.data;
            });
        };
    	
        var init = function () {
        	getSADashboardInfo();
        	console.log('SA Dashboard Controller');        	
         };
        
        init();
        
    };    
    
    app.register.controller('saDashboardController', ['$scope', '$location', '$filter', '$log',
    'constantService', 'authorizationService', 'saDashboardService', saDashboardController]);
});


