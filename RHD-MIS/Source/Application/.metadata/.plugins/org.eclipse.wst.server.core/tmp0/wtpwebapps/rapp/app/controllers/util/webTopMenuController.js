
'use strict';

define(['app'], function (app) {
    
	 var webTopMenuController = function ($rootScope, $scope, $log, $http, $location, $route, 
		navigationService, configurationService, localStorageService,constantService, localizesignInService) {
		 
		var userInfo, promis;
		
		$scope.navigatePage = function(url){
				navigationService.menuNavigation(url);
		};
		 
		var init = function () {
			$http.get('config/menu.json').success(function(data) {
				$scope.webLeftMenus = data;
				$scope.loginObj = $scope.webLeftMenus[$scope.webLeftMenus.length-1];
				$scope.webLeftMenus.pop();
		    });
	    }; 
	    
	    init();
		 
	 };    
	 
	 app.controller('webTopMenuController', ['$rootScope', '$scope', '$log', '$http', '$location', '$route', 
     'navigationService', 'configurationService', 'localStorageService', 'constantService', 'localize', webTopMenuController]);
	
});

