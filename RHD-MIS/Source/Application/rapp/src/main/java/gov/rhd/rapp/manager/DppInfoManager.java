package gov.rhd.rapp.manager;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;

public interface DppInfoManager extends Manager {

	public ResponseBean saveDpp(ResponseBean response, DppInfoBean model);
	
}
