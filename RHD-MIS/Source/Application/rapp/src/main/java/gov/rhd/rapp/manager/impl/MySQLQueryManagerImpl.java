
package gov.rhd.rapp.manager.impl;

import java.util.Date;

import gov.rhd.rapp.manager.QueryManager;
import gov.rhd.rapp.util.Table;

@SuppressWarnings("unused")
public class MySQLQueryManagerImpl implements QueryManager {

	@Override
	public String getUpdateMetaPropertyByOidSql() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMetaPropertyByOidSQL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAllRoleSql() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertLoginLogSql() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserSql() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserByLoginIDSql() {
		//String sql = " SELECT * FROM Login where loginID = ? ";
		
		String sql = "SELECT loginTable.OID, loginTable.loginID, loginTable.userPassword, "
				+ " loginTable.roleOID, loginTable.firstName, "
				+ " roleTable.roleDescription, roleTable.menuJSON "
				+ " FROM Login loginTable "
				+ " JOIN role roleTable ON loginTable.roleOID = roleTable.OID "
				+ " WHERE loginTable.loginID = ? ";	
		
		return sql;
	}

	@Override
	public String getInsertDppInfoSql() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveDppInfoSql() {
		String sql = "insert into "+Table.DPP_INFO +"(oid, ProjectName, District, Zcd, "
				+ " ProjectType, FundType, BrProject, ProcessedByPlan, "
				+ " ReceivedByPlanning, Status, Budget, Description, "
				+ " dppDocumentName, dppDocumentLocation, dppDocumentFileName, receiveDate) " +
				" values(?, ?, ?, ?,    ?, ?, ?, ?,    ?, ?, ?, ?,    ?, ?, ?, ? )";
		return sql;
	}

	@Override
	public String getDppInfoSql() {
		String sql = "SELECT * FROM "+Table.DPP_INFO + " ";
		return sql;
	}

	@Override
	public String submitDppApprovalSql() {
		String sql = "insert into "+Table.DPP_APPROVAL +"(oid, supDocumentName, supDocumentLocation, "
				+ " supDocumentFileName, approvalDescription, receiveDate, submissionDate) " + 
				" values( ?, ?, ?,   ?, ?, ?, ?)";
		return sql;

	}

	/*@Override
	public String updateDppSql() {
		String sql = "UPDATE dpp SET ProjectName= ?, District= ?, Zcd= ?, ProjectType= ?,"
				+ " FundType= ?, BrProject= ?, ProcessedByPlan= ?, processbyplan= ?,"
				+ " ReceivedByPlanning= ?, Status= ?, Budget= ?, Description= ?,"
				+ " dppDocumentName= ? dppDocumentLocation= ?, dppDocumentFileName= ?, receiveDate= ? WHERE oid= ?";

		return sql;
	}*/
	
	
	

	

	

}
