package gov.rhd.rapp.bean;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public abstract class AbstractBean implements Serializable {
	
	protected String UTIL_DATE_FORMAT = "dd/MM/yyyy";
	protected String operation;
	protected String oid;
	protected Date createdOn;
	protected String createdBy;
	protected Date editedOn;
	protected String editedBy;
	
	private LoginBean loginBean;
	
	public LoginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	abstract public Date getCreatedOn();
	abstract public void setCreatedOn(Date createdOn);
	
	abstract public String getCreatedBy();
	abstract public void setCreatedBy(String createdBy);
	
	abstract public Date getEditedOn();
	abstract public void setEditedOn(Date editedOn);
	
	abstract public String getEditedBy();
	abstract public void setEditedBy(String editedBy);
	
	abstract public String getOperation();
	abstract public void setOperation(String operation);
	
	abstract public String getOid();
	abstract public void setOid(String oid);
	

}
