package gov.rhd.rapp.rest.service;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppListManager;
import gov.rhd.rapp.util.Constant;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/dppList")
public class DppListService {	

	@Context ServletContext servletContext;
	@Context HttpServletRequest request;
	
	private DppListManager dppListManager;

	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ResponseBean postObject(DppInfoBean model) {
		
		ResponseBean response = new ResponseBean();
		if(model.getOperation().equalsIgnoreCase(Constant.GET_ALL)){
			response = dppListManager.getDpps(response, model);
		} 
		
		return response;
	}
	// Setter
	public void setDppListManager(DppListManager dppListManager) {
		this.dppListManager = dppListManager;
	}
	
}
	
	

