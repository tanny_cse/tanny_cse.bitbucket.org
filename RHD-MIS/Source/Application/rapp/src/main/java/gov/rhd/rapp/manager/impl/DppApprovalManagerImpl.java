package gov.rhd.rapp.manager.impl;

import java.util.Date;

import org.apache.log4j.Logger;

import gov.rhd.rapp.bean.DppApprovalBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppApprovalManager;

public class DppApprovalManagerImpl extends ManagerImpl implements DppApprovalManager {
	
	private Logger _logger = Logger.getLogger(this.getClass());
	
	@Override
	public ResponseBean submitDpp(ResponseBean response, DppApprovalBean model) {
		String sql = queryManager.submitDppApprovalSql();
		try {
			
			Object[] param = new Object []{
					idGenerator.generateId(), model.getSupDocumentName(), model.getSupDocumentLocation(),
					model.getSupDocumentFileName(), model.getApprovalDescription(),
					new Date(),
					new Date()
					
			};
			springJdbcDao.submitObject(sql, param);
			
		} catch (Exception e){
			e.printStackTrace();
			_logger.error("An Exception occured while Submit User : ", e);
			return response;
			//response.setCode(Code.Us1000);
		}
		
		response.setSuccess(true);
		return response;
	}

}
