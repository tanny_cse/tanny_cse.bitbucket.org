  package gov.rhd.rapp.manager.impl;

import org.apache.log4j.Logger;

import gov.rhd.rapp.bean.LoginBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.LoginManager;

public class LoginManagerImpl extends ManagerImpl implements LoginManager {

	private Logger _logger = Logger.getLogger(this.getClass());
	
	@Override
	public ResponseBean doLogin(ResponseBean response, LoginBean model) {		
		
		try {
			LoginBean loginBean = dataProviderUtil.getLoginByLoginID(springJdbcDao, queryManager.getUserByLoginIDSql(), model.getLoginID());
			if(loginBean == null){
				_logger.warn("No found Login for : "+model.getLoginID());
				//response.setCode(Code.Nl1000);
				return response;
			}
			
			if(!loginBean.getUserPassword().equals(model.getUserPassword())){
				_logger.warn("Password not match for : "+ model.getLoginID());
				//response.setCode(Code.Pw1000);
				return response;
			}
			
/*			if(!loginBean.getStatus().equalsIgnoreCase(ACTIVE_SHORT)){
				_logger.warn("Inactive user for : "+model.getLoginID());
				response.setCode(Code.Ia1000);
				return response;
			}*/

/*			loginBean.setSessionId(idGenerator.getSessionId());
			loginBean.setPassword(null);*/
			response.setData(loginBean);
			//sessionManager.setUserInSession(loginBean);
			//LoginLog loginLog = new LoginLog(loginBean, request);
			//simpleJdbcDao.saveObject(loginLog, Table.LOGIN_LOG);
			_logger.info("Successfully Login : "+ model.getLoginID());
		} catch (Exception e){			
			_logger.error("An Exception occured while try to Login : ", e);
		}
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseBean doLogout(ResponseBean resBean, LoginBean model) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	

}
