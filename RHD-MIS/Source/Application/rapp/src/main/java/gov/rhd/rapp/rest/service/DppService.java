package gov.rhd.rapp.rest.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppInfoManager;
import gov.rhd.rapp.util.Constant;


@Path("/dppInfo")
public class DppService {
	
	@Context ServletContext servletContext;
	@Context HttpServletRequest request;

	private DppInfoManager dppInfoManager;
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
	public ResponseBean postObject(DppInfoBean model) {
		
		ResponseBean response = new ResponseBean();
	
		if(model.getOperation().equalsIgnoreCase(Constant.SAVE)){
			response = dppInfoManager.saveDpp(response, model); // Save Dpp
		}	
		return response;
	}

	public void setDppInfoManager(DppInfoManager dppInfoManager) {
		this.dppInfoManager = dppInfoManager;
	}
	
	
	
	
	
	
	


}
