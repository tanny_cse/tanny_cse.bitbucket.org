package gov.rhd.rapp.rest.service;

import gov.rhd.rapp.bean.DppApprovalBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppApprovalManager;
import gov.rhd.rapp.util.Constant;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/dppApp")
public class DppApprovalService {

	@Context
	ServletContext servletContext;
	@Context
	HttpServletRequest request;

	private DppApprovalManager dppApprovalManager;

	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseBean postObject(DppApprovalBean model) {

		ResponseBean response = new ResponseBean();

		if(model.getOperation().equalsIgnoreCase(Constant.SUBMIT)){
			response = dppApprovalManager.submitDpp(response, model);
		}
		return response;
	}
	

	public void setDppApprovalManager(DppApprovalManager dppApprovalManager) {
		this.dppApprovalManager = dppApprovalManager;
	}

}
