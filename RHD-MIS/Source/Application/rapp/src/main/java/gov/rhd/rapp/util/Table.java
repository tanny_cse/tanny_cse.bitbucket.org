package gov.rhd.rapp.util;

public abstract class Table {
	
	public static final String META_PROPERTY = "MetaProperty";
	public static final String ROLE = "Role";
	public static final String LOGING = "Loging";
	public static final String LOGIN_LOG = "LoginLog";
	public static final String AUDIT_LOG = "AuditLog";
	public static final String MEETING = "Meeting";	
	public static final String DPP_INFO = "DppInfo";
	public static final String DPP_APPROVAL = "DppApproval";	

}
