package gov.rhd.rapp.bean;

import java.util.Date;

@SuppressWarnings("serial")
public class RoleBean extends AbstractBean {
	
	private String roleDescription;
	private String menuJSON;	

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public String getMenuJSON() {
		return menuJSON;
	}

	public void setMenuJSON(String menuJSON) {
		this.menuJSON = menuJSON;
	}

	@Override
	public String getOperation() {
		return operation;
	}

	@Override
	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String getOid() {
		return oid;
	}

	@Override
	public void setOid(String oid) {
		this.oid = oid;
	}

	@Override
	public Date getCreatedOn() {
		return createdOn;
	}

	@Override
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public Date getEditedOn() {
		return editedOn;
		
	}

	@Override
	public void setEditedOn(Date editedOn) {
		this.editedOn = editedOn;
	}

	@Override
	public String getEditedBy() {
		return editedBy;
	}

	@Override
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
		
	}

}
