package gov.rhd.rapp.bean;

import java.util.Date;

@SuppressWarnings("serial")
public class DppApprovalBean extends AbstractBean{
	
	//private String dppInfoOID;
	private String supDocumentName;
	private String supDocumentLocation;
	private String supDocumentFileName;
	private String approvalDescription;
	private Date receiveDate;
	private Date submissionDate;

	/*public String getDppInfoOID() {
		return dppInfoOID;
	}

	public void setDppInfoOID(String dppInfoOID) {
		this.dppInfoOID = dppInfoOID;
	}*/

	public String getSupDocumentName() {
		return supDocumentName;
	}

	public void setSupDocumentName(String supDocumentName) {
		this.supDocumentName = supDocumentName;
	}

	public String getSupDocumentLocation() {
		return supDocumentLocation;
	}

	public void setSupDocumentLocation(String supDocumentLocation) {
		this.supDocumentLocation = supDocumentLocation;
	}

	public String getSupDocumentFileName() {
		return supDocumentFileName;
	}

	public void setSupDocumentFileName(String supDocumentFileName) {
		this.supDocumentFileName = supDocumentFileName;
	}

	public String getApprovalDescription() {
		return approvalDescription;
	}

	public void setApprovalDescription(String approvalDescription) {
		this.approvalDescription = approvalDescription;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	@Override
	public String getOperation() {
		return operation;
	}

	@Override
	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String getOid() {
		return oid;
	}

	@Override
	public void setOid(String oid) {
		this.oid = oid;
	}

	@Override
	public Date getCreatedOn() {
		return createdOn;
	}

	@Override
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public Date getEditedOn() {
		return editedOn;
		
	}

	@Override
	public void setEditedOn(Date editedOn) {
		this.editedOn = editedOn;
	}

	@Override
	public String getEditedBy() {
		return editedBy;
	}

	@Override
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
		
	}

}
