package gov.rhd.rapp.util;

import org.apache.log4j.Logger;

import gov.rhd.rapp.bean.LoginBean;
import gov.rhd.rapp.dao.SpringJdbcDao;

public class DataProviderUtil {

	private Logger _logger = Logger.getLogger(this.getClass());
	
	public LoginBean getLoginByLoginID(SpringJdbcDao springJdbcDao, String sql, String loginID) throws Exception {
		
		_logger.info("=||=||=||=||= getLoginByLoginID =||=||=||=||=");
		
		LoginBean loginBean = null;
		try {
			loginBean = (LoginBean) springJdbcDao.getObject(sql, new Object[]{loginID}, LoginBean.class);
		} catch (Exception e){			
			_logger.error("Exception occurred while getting Login Info by LoginID : ", e);
			throw e;
		}
		return loginBean;
	}
}
