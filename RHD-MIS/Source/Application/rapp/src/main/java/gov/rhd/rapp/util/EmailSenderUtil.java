package gov.rhd.rapp.util;

import org.apache.log4j.Logger;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class EmailSenderUtil implements Constant {
	
	private Logger _logger = Logger.getLogger(this.getClass());
	private MailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	
	public void sendMail(String subject, String content, String[] toMailList) throws Exception {
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(content);
		simpleMailMessage.setTo(toMailList);
		try {
			mailSender.send(simpleMailMessage);
		} catch(Exception m){
			_logger.error("Exception occurred while sending mail : ", m);
			throw m;
		}
	}
	
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}
	
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
}
