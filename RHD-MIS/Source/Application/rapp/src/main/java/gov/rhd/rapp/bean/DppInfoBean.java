package gov.rhd.rapp.bean;

import java.util.Date;

@SuppressWarnings("serial")
public class DppInfoBean extends AbstractBean {
	
	private String projectName;
	private String district;
	private String zCD;
	private String projectType;
	private String fundType;
	private String brProject;
	private String processedByPlan;
	private Date receivedByPlanning;
	private String status;
	private double budget;
	private String description;
	private String dppDocumentName;
	private String dppDocumentLocation;
	private String dppDocumentFileName;
	private Date receiveDate;
	private Date submissionDate;

	public DppInfoBean() { }

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getzCD() {
		return zCD;
	}

	public void setzCD(String zCD) {
		this.zCD = zCD;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getBrProject() {
		return brProject;
	}

	public void setBrProject(String brProject) {
		this.brProject = brProject;
	}

	public String getProcessedByPlan() {
		return processedByPlan;
	}

	public void setProcessedByPlan(String processedByPlan) {
		this.processedByPlan = processedByPlan;
	}	

	public Date getReceivedByPlanning() {
		return receivedByPlanning;
	}

	public void setReceivedByPlanning(Date receivedByPlanning) {
		this.receivedByPlanning = receivedByPlanning;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDppDocumentName() {
		return dppDocumentName;
	}

	public void setDppDocumentName(String dppDocumentName) {
		this.dppDocumentName = dppDocumentName;
	}

	public String getDppDocumentLocation() {
		return dppDocumentLocation;
	}

	public void setDppDocumentLocation(String dppDocumentLocation) {
		this.dppDocumentLocation = dppDocumentLocation;
	}

	public String getDppDocumentFileName() {
		return dppDocumentFileName;
	}

	public void setDppDocumentFileName(String dppDocumentFileName) {
		this.dppDocumentFileName = dppDocumentFileName;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
	
	@Override
	public String getOperation() {
		return operation;
	}

	@Override
	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String getOid() {
		return oid;
	}

	@Override
	public void setOid(String oid) {
		this.oid = oid;
	}

	@Override
	public Date getCreatedOn() {
		return createdOn;
	}

	@Override
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public Date getEditedOn() {
		return editedOn;
		
	}

	@Override
	public void setEditedOn(Date editedOn) {
		this.editedOn = editedOn;
	}

	@Override
	public String getEditedBy() {
		return editedBy;
	}

	@Override
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
		
	}
	
	

}
