package gov.rhd.rapp.rest.service;

import gov.rhd.rapp.bean.LoginBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.LoginManager;
import gov.rhd.rapp.util.Constant;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype")
@Path("/login")
public class LoginService {
	@Context ServletContext servletContext;
	@Context HttpServletRequest request;
	//
	private LoginManager loginManager;
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ResponseBean postObject(LoginBean model) {
		ResponseBean response = new ResponseBean();
		if(model.getOperation().equalsIgnoreCase(Constant.LOGIN)){
			response = loginManager.doLogin(response, model);
		} else if(model.getOperation().equalsIgnoreCase(Constant.LOGOUT)){
			response = loginManager.doLogout(response, model);
		}
		
		return response;
	}

	public void setLoginManager(LoginManager loginManager) {
		this.loginManager = loginManager;
	}
	

}
