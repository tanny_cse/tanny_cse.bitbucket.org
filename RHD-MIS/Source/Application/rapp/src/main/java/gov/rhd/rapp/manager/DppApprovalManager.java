package gov.rhd.rapp.manager;

import gov.rhd.rapp.bean.DppApprovalBean;
import gov.rhd.rapp.bean.ResponseBean;

public interface DppApprovalManager {
	
	public ResponseBean submitDpp(ResponseBean response, DppApprovalBean model);

}
