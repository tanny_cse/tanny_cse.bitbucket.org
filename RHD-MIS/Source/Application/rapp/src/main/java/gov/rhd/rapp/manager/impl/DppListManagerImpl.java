package gov.rhd.rapp.manager.impl;

import java.util.List;

import org.apache.log4j.Logger;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppListManager;

public class DppListManagerImpl extends ManagerImpl implements DppListManager {
	
	private Logger _logger = Logger.getLogger(this.getClass());

	@Override
	public ResponseBean getDpps(ResponseBean response, DppInfoBean model) {
		try {
			@SuppressWarnings("unchecked")
			List<DppInfoBean> dppList = springJdbcDao.getObjectList(queryManager.getDppInfoSql(), null, DppInfoBean.class);
			if(dppList != null && !dppList.isEmpty()){
				response.setData(dppList);
				response.setSuccess(true);
				//response.setCode(Code.Su1002);
			}
			_logger.info("Successfully Load All User");
		} catch (Exception e){			
			_logger.error("An Exception occured while get all DppInfo : ", e);
			//response.setCode(Code.Nd1000);
		}
		return response;
	}

	
}
