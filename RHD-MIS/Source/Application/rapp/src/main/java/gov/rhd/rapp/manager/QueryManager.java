package gov.rhd.rapp.manager;


public interface QueryManager {

/////////////////////////Setup, Meta Data, Role /////////////////////////////////////////////////////////////////////////////////////////////////	
	public String getUpdateMetaPropertyByOidSql();
	
	public String getMetaPropertyByOidSQL();
	
	public String getAllRoleSql();

////////////////////////////////////////////////////////Login, LoginLog ///////////////////////////////////////////////////////////////////////////////////////////
	public String getInsertLoginLogSql();
	
	public String getUserSql();
	
	public String getUserByLoginIDSql();
	
////////////////////////////////////////////// DPP ////////////////////////////////////////////////////////
	public String getInsertDppInfoSql();
	public String getDppInfoSql();
	public String saveDppInfoSql();
	public String submitDppApprovalSql();

	
	

}
