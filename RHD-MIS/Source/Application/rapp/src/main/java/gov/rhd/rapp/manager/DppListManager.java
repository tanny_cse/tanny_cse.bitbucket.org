package gov.rhd.rapp.manager;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;

public interface DppListManager extends Manager {
	
	 public ResponseBean getDpps(ResponseBean response, DppInfoBean model);

	
}
