package gov.rhd.rapp.manager.impl;

import gov.rhd.rapp.dao.SimpleJdbcDao;
import gov.rhd.rapp.dao.SpringJdbcDao;
import gov.rhd.rapp.manager.Manager;
import gov.rhd.rapp.manager.QueryManager;
import gov.rhd.rapp.manager.SessionManager;
import gov.rhd.rapp.util.DataProviderUtil;
import gov.rhd.rapp.util.IdGenerator;

public class ManagerImpl implements Manager {

	protected SessionManager sessionManager;
	protected QueryManager queryManager;
	protected IdGenerator idGenerator;
	protected SpringJdbcDao springJdbcDao;
    protected SimpleJdbcDao simpleJdbcDao;
    protected DataProviderUtil dataProviderUtil;
   
	
	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
	
	public void setQueryManager(QueryManager queryManager) {
		this.queryManager = queryManager;
	}
	
	public void setIdGenerator(IdGenerator idGenerator) {
		this.idGenerator = idGenerator;
	}
	
	public void setSpringJdbcDao(SpringJdbcDao springJdbcDao) {
		this.springJdbcDao = springJdbcDao;
	}
	
	public void setSimpleJdbcDao(SimpleJdbcDao simpleJdbcDao) {
		this.simpleJdbcDao = simpleJdbcDao;
	}

	public void setDataProviderUtil(DataProviderUtil dataProviderUtil) {
		this.dataProviderUtil = dataProviderUtil;
	}

	


}
