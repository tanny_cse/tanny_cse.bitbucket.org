package gov.rhd.rapp.manager.impl;

import java.util.Date;

import org.apache.log4j.Logger;

import gov.rhd.rapp.bean.DppInfoBean;
import gov.rhd.rapp.bean.ResponseBean;
import gov.rhd.rapp.manager.DppInfoManager;

public class DppInfoManagerImpl extends ManagerImpl implements DppInfoManager {
	
	private Logger _logger = Logger.getLogger(this.getClass());

	@Override
	public ResponseBean saveDpp(ResponseBean response, DppInfoBean model) {
		
		String sql = queryManager.saveDppInfoSql();
		try {
			
			Object[] param = new Object []{
					idGenerator.generateId(), model.getProjectName(), model.getDistrict(), model.getzCD(),
					model.getProjectType(), model.getFundType(), model.getBrProject(), model.getProcessedByPlan(),
					model.getReceivedByPlanning(), model.getStatus(), model.getBudget(), model.getDescription(), model.getDppDocumentName(),
					model.getDppDocumentLocation(),
					model.getDppDocumentFileName(),
					new Date()

					
			};
			springJdbcDao.saveObject(sql, param);
			
		} catch (Exception e){
			e.printStackTrace();
			_logger.error("An Exception occured while Save User : ", e);
			return response;
			//response.setCode(Code.Us1000);
		}
		
		response.setSuccess(true);
		return response;
	}

} // end of class
