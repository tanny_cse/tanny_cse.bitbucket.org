
'use strict';

define(['app', 'services/utils/configurationService'], function (app) {

    var adminDashboardService = function ($resource, $q, configurationService, constantService) {
    	
    	var adminDashboardResource, delay;
        
    	adminDashboardResource = $resource(configurationService.allDashboardInfo, {}, {
    		getAdminDashboardInfo: { method: 'POST'}
        });
        
        this.getAdminDashboardInfo = function (obj) {
            delay = $q.defer();
            adminDashboardResource.getAdminDashboardInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };        
    };
    
    app.service('adminDashboardService', ['$resource', '$q', 'configurationService', 'constantService', adminDashboardService]);
});







