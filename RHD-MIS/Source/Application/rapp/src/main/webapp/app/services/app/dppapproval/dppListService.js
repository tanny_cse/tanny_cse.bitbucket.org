﻿
'use strict';

define(['app'], function (app) {

	var dppListService = function ($rootScope, $resource, $q, constantService, configurationService, messageService) {
		
		var dppListResource, delay;
		
		dppListResource = $resource(configurationService.DppInfoList, {}, {
			postObject: { method: 'POST' },
			getDppInfo: { method: 'POST' }
			//deleteData : {method : 'POST'}
		});
        
        this.postObject = function (obj) {
            delay = $q.defer();
            dppListResource.postObject(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };
		
        
        this.getDppInfolist = function (obj) {
            delay = $q.defer();
            dppListResource.getDppInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };

    };
    
    app.service('dppListService', ['$rootScope', '$resource', '$q', 'constantService', 'configurationService', 
    'messageService', dppListService]);

});


