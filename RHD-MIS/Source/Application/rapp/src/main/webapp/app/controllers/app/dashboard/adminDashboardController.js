
'use strict';

define(['app'], function (app) {
    
	var adminDashboardController = function ($scope, $location, $filter, $log, $route, constantService, 
			authorizationService, adminDashboardService) {

    	var userInfo, promis;
    	$scope.adminDashboardInfo = [];
		
    	var getAdminDashboardInfo = function () {
        	var userObj = { operation : constantService.GetAdminDashboardInfo, loginBean : authorizationService.getUserInfo() };
        	promis = adminDashboardService.getAdminDashboardInfo(userObj);
            promis.then(function (data) {
            	if (!data.success) {
                    return;
                }
            	$scope.adminDashboardInfo = data.data;
            });
        };

        var init = function () {
        	getAdminDashboardInfo();
        	console.log('Admin Dashboard Controller');        	
        };
        
        init();

    };    
    
    app.register.controller('adminDashboardController', ['$scope', '$location', '$filter', '$log', '$route',
    'constantService', 'authorizationService', 'adminDashboardService', adminDashboardController]);
});


