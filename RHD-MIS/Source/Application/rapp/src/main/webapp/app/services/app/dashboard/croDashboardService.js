
'use strict';

define(['app', 'services/utils/configurationService'], function (app) {

    var croDashboardService = function ($resource, $q, configurationService, constantService) {
 debugger;   	
    	var croDashboardResource, delay;
        
    	croDashboardResource = $resource(configurationService.allDashboardInfo, {}, {
    		getCRODashboardInfo: { method: 'POST'}
        });
        
        this.getCRODashboardInfo = function (obj) {
            delay = $q.defer();
            croDashboardResource.getCRODashboardInfo(obj, function (data) {
                delay.resolve(data);
            }, function () {
                delay.reject('Unable to fetch..');
            });
            return delay.promise;
        };        
    };
    
    app.service('croDashboardService', ['$resource', '$q', 'configurationService', 'constantService', croDashboardService]);
});







