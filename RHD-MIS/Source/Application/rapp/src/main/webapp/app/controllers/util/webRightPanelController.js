
'use strict';

define(['app'], function (app) {
    
	 var webRightPanelController = function ($rootScope, $scope, $log, $http, $location, $route, 
		navigationService, configurationService, localStorageService,constantService, localize, 
		modalService, dataService) {
		 
		var userInfo, promis;
		
		$scope.show = function (notice) {
        	var url = '';
        	var noticeboarditem = '';
        	var headerText = '';
        	headerText = notice.shortDescription;
    		noticeboarditem = notice.filePath;
			
        	var modalOptions = {
				closeButtonText: 'No',
      	        actionButtonText: 'Close',
      	        headerText: headerText,
      	        bodyText: noticeboarditem
    		};
			var modalDefaults = {
    			templateUrl: 'app/partials/reportModal.html',
    	        size: 'lg'
    	    };
			
			modalService.showModal(modalDefaults, modalOptions).then(function (result) {
				if(result == 'cancel'){
					return;
				}
         	});
	        
        };
		
        $scope.navigatePage = function(url){
			navigationService.menuNavigation(url);
		}
    	
		var getAllNotices = function () {			
			var noticeObj = { operation : constantService.GetAllNotice};
        	promis = dataService.postObject(noticeObj);
            promis.then(function (data) {
    			if (!data.success) {
                    return;
                }
            	$scope.notices = data.data;
            });
        };
        
        
        
		var init = function () {
			
			getAllNotices();
			
			/*$http.get('config/latestnews.json').success(function(data) {
				$scope.latestNews = data;
		    });
			
			$http.get('config/notice.json').success(function(data) {
				$scope.notice = data;
		    });*/
			
			$http.get('config/upcomingevents.json').success(function(data) {
				$scope.upcomingevents = data;
		    });
			
			$http.get('config/administration.json').success(function(data) {
				$scope.administrations = data;
		    });
	    }; 
	    
	    init();
		 
	 };    
	 
	 app.controller('webRightPanelController', ['$rootScope', '$scope', '$log', '$http', '$location', '$route', 
     'navigationService', 'configurationService', 'localStorageService', 'constantService', 'localize','modalService', 
     'dataService', webRightPanelController]);
	
});

