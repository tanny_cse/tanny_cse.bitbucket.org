﻿
require.config({
    baseUrl: 'app',
    urlArgs: 'v=1.0'
});

require(
    [
        'app',
        'directives/ngEnter',
        'directives/decimalMask',
        'directives/integerMask',
        'directives/numberWithDecimal',
        'directives/highChart',
        'directives/myCalendar',
        'services/utils/alertService',
        'services/utils/authorizationService',
        'services/utils/configurationService',
        'services/utils/confirmationService',
        'services/utils/constantService',
        'services/utils/dataService',
        'services/utils/informationService',
        'services/utils/inputValidationService',
        'services/utils/languageService',
        'services/utils/loadService',
        'services/utils/localStorageService',
        'services/utils/menuService',
        'services/utils/messageService',
        'services/utils/modalService',
        'services/utils/navigationService',
        'services/utils/routeResolver',
        'services/app/dashboard/saDashboardService',
        'services/app/dashboard/adminDashboardService',
        'services/web/security/signInService',
        'services/app/dppapproval/dppService',
        'services/app/dppapproval/dppListService',
        'filters/userFilter',
        'controllers/util/messageController',
        'controllers/util/webHeaderController',
        'controllers/util/webTopMenuController',
        'controllers/util/webFooterController',
        'controllers/util/appTopMenuController',
        'controllers/util/appHeaderController',
    ],
function () {
    angular.bootstrap(document, ['mbmsApp']);
});

