/*
Create database of oppofinancedb
*/
DROP DATABASE IF EXISTS rhddpp;
CREATE DATABASE rhddpp;

/*
Connect as rhddpp
*/
USE rhddpp;

/*
drop objects if existing;
*/
DROP TABLE IF EXISTS	LoginLog;
DROP TABLE IF EXISTS	AuditLog;


-- Security, Setup and Meta Data
DROP TABLE IF EXISTS	Login;
DROP TABLE IF EXISTS	Role;
DROP TABLE IF EXISTS	MetaProperty;

/*
	MetaProperty
	This table contains meta properties (like setup data, DDL data etc.) in JSON format.
	
	OID							: Primary key
	metaPropertyDescription		: Description of meta property
	valueJSON					: JSON containing setup data
*/
CREATE TABLE 					MetaProperty
(
OID 							VARCHAR(32) 									NOT NULL,
metaPropertyDescription			VARCHAR(512),	
valueJSON						LONGTEXT	 									NOT NULL,
createdOn						DATETIME,
createdBy						VARCHAR(64),
editedOn						DATETIME,
editedBy						VARCHAR(64),
CONSTRAINT 						pk_MetaProperty 								PRIMARY KEY 	(OID)
);

/*
	Role
	This table contains various roles of the users.
	
	OID							: Primary key
	roleDescription				: Description of role
	menuJSON					: JSON containing menu and sub menus
*/
CREATE TABLE 					Role
(
OID 							VARCHAR(32) 									NOT NULL,
roleDescription 				VARCHAR(512),
menuJSON 						LONGTEXT										NOT NULL,
CONSTRAINT 						pk_Role 										PRIMARY KEY 	(OID)
);

/*
	Login
	This table contains every user's login information (user credential).
	
	OID							: Primary key
	loginIDAccountNo			: System generated A/c No. for the member
	loginIDEmailAddress			: Email address of the member for notifying the member while completion of A/c creation
	userPassword				: Password of the member	
	roleOID						: Role of the member
	firstName					: First name of the member
	lastName					: Last name of the member (not mandatory)
	statusOfUser				: For user Active or Block, but for members Active, Lock, Block, Terminate
	userOrMember				: Identification - User or Member
	securityQuestion   			: Security question
	securityQuestionAnswer   	: Answer of security question
*/
CREATE TABLE					Login
(
OID								VARCHAR(32)										NOT NULL,
loginID			 				VARCHAR(64)										NOT NULL		UNIQUE,
userPassword  					VARCHAR(32)										NOT NULL,
roleOID							VARCHAR(32)										NOT NULL,
firstName 						VARCHAR(128),
lastName						VARCHAR(128),
statusOfUser    				VARCHAR(64),
securityQuestion   				VARCHAR(256),
securityQuestionAnswer   		VARCHAR(256),			
createdOn						DATETIME,
createdBy						VARCHAR(64),
editedOn						DATETIME,
editedBy						VARCHAR(64),
CONSTRAINT						pk_Login										PRIMARY KEY		(OID)
); 


CREATE TABLE					DppInfo
(
OID 							VARCHAR(32)										NOT NULL,
projectName  				    VARCHAR(128)									NOT NULL,
district  			            VARCHAR(128)									NOT NULL,
zCD 		 		            VARCHAR(128)								    NOT NULL,
projectType 			        VARCHAR(128)									NOT NULL,
fundType			   		    VARCHAR(128)								    NOT NULL,
brProject 		                VARCHAR(128),
processedByPlan 		        VARCHAR(128),
receivedByPlanning 	            DATETIME,
status                       	VARCHAR(128),
budget			                DOUBLE,
description            		    VARCHAR(512),
dppDocumentName			  		VARCHAR(128),
dppDocumentLocation				VARCHAR(256),
dppDocumentFileName    			VARCHAR(128),
receiveDate 					DATETIME,
submissionDate 					DATETIME,
createdOn						DATETIME,
createdBy						VARCHAR(64),
editedOn						DATETIME,
editedBy						VARCHAR(64),
CONSTRAINT						pk_DppInfo										PRIMARY KEY		(OID)
);


CREATE TABLE					DppApproval
(
oid 							VARCHAR(32)										NOT NULL,
DppInfoOID						VARCHAR(32)										NOT NULL,
supDocumentName			  		VARCHAR(128),
supDocumentLocation				VARCHAR(256),
supDocumentFileName    			VARCHAR(128),
approvalDescription            	VARCHAR(512),
receiveDate 					DATETIME,
submissionDate 					DATETIME,
createdOn						DATETIME,
createdBy						VARCHAR(64),
editedOn						DATETIME,
editedBy						VARCHAR(64),
CONSTRAINT						pk_DppApproval									PRIMARY KEY		(OID)
);


CREATE TABLE 					LoginLog
(
OID								VARCHAR(64),
loginOID						VARCHAR(32)										NOT NULL,
roleOID							VARCHAR(32)										NOT NULL,
loginID							VARCHAR(32)										NOT NULL,
loginIDAccountNo 				VARCHAR(32)										NOT NULL,
loginIDEmailAddress 			VARCHAR(128)									NOT NULL,
serverLocation					LONGTEXT,
loginTime						DATETIME										NOT NULL,
logoutTime						DATETIME,
loginStatus						VARCHAR(64)										NOT NULL		DEFAULT					'Login',
CONSTRAINT						pk_LoginLog										PRIMARY KEY		(OID)
);


