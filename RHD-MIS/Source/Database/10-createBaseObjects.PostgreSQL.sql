/*
connect as mbms and create the following tables
*/

/*
drop objects if existing;
*/

-- Audit
drop table if exists	LoginLog;
drop table if exists	AuditLog;

-- Security
drop table if exists	Login;
drop table if exists	Role;

-- Meta Data
drop table if exists	MetaProperty;

/*
------------------------------------------------------------------------------
M E T A   D A T A
------------------------------------------------------------------------------
*/

/*
MetaProperty
valueJSON						: []
description						: Description of Value JSON
*/
create table 					MetaProperty
(
OID 							varchar(64) 						not null,
valueJSON						varchar(1024) 						not null,
description						varchar(64),	
-- For Audit
createdOn						timestamp							not null		default 						current_date,
createdBy						varchar(32)							not null		default 						user,
editedOn						timestamp,
editedBy						varchar(32),
constraint 						pk_MetaProperty 					primary key 	(OID)
);


/*
------------------------------------------------------------------------------
S E C U R I T Y
------------------------------------------------------------------------------
*/

/*
roleID  						: role ID
roleDescription 				: description
menuJSON 						: [{"topmenuid" : "Security_TopMenu", "leftmenuids" : ["Users_Security_Security"]}]
*/
create table 					Role
(
roleID 							varchar(64) 						not null,
roleDescription 				varchar(64) 						not null,
menuJSON 						text,
constraint 						pk_Role 							primary key 	(roleID)
);

/*
Login
roleID							: Role ID from Role table which type is primary
password						: Login Password
status							: (A)Active, (I)Inactive
name							: Login User name
email							: Login User email id, it should be use for notification of the user like change password
phoneNo							: Login User Phone Number
roleJSON						: Role JSON for role ids which type is secondary 
*/
create table					Login
(
loginID							varchar(32)							not null,
roleID							varchar(64)							not null,
password						varchar(100)						not null,
status							varchar(2)							not null,
name							varchar(100)						not null,
imagePath						varchar(100),
-- For Audit
createdOn						timestamp 							not null		default 						current_date,
createdBy						varchar(32)							not null		default 						user,
editedOn						timestamp,
editedBy						varchar(32),
constraint						fk_roleID_Login						foreign key		(roleID)						references	Role(roleID),
constraint						ck_status_Login						check			(status = 'A' or status = 'I'),
constraint						pk_Login							primary key		(loginID)
); 

/*
------------------------------------------------------------------------------
M A S T E R   D A T A
------------------------------------------------------------------------------
*/



/*
------------------------------------------------------------------------------
A U D I T
------------------------------------------------------------------------------
*/

/*
Audit Log for tracing every change in every table
OID								: Primary key 
tableName						: name of the table for which the trace is
rowKey							: OID of the row for which the trace is
timeOfAction					: time of action
actionType 						: (I)nsert, (E)dit, (D)elete
actionSource 					: (U)ser, (S)ystem, (A)dmin
actionUser 						: Login username if from user end, or System if from system or A if by Admin
rowImageBefore 					: JSON representation of all rows (valid for update and delete), JSON must be line by line for diff capability
rowImageAfter 					: JSON representation of all rows (valid for insert and after), JSON must be line by line for diff capability
*/
create table					AuditLog
(
OID								varchar(64)							not null,
tableName						varchar(64)							not null,
rowKey							varchar(64)							not null,
timeOfAction					timestamp							not null			default							current_date,
actionType						varchar(2)							not null,
actionSource					varchar(2)							not null,
actionUser						varchar(64)							not null,
rowImageBefore					text,
rowImageAfter					text,
constraint						ck_actionType_AuditLog				check				(actionType = 'I' or actionType = 'E' or actionType = 'D'),
constraint						ck_actionSource_AuditLog 			check				(actionSource = 'U' or actionSource = 'S' or actionSource = 'A'),
constraint						pk_AuditLog							primary key 		(OID)
);


/*
loginStatus						: Login, Logout
*/
create table 					LoginLog
(
oid								varchar(64),
loginID							varchar(64)							not null,
roleID							varchar(64)							not null,
ipAddress						varchar(50),
loginTime						timestamp							not null,
logoutTime						timestamp,
loginStatus						varchar(20)							not null,
constraint						pk_LoginLog							primary key		(oid)
);

