-- connect as mbms and insert the following data into tables

-- NOTE: Oracle	TO_DATE('2012-03-01 17:51:23', 'YYYY-MM-DD HH24:MI:SS')
-- NOTE: MySQL	STR_TO_DATE('2012-03-01 17:51:23', '%Y-%m-%d %H:%i%s')

--set define off

delete from	LoginLog;
delete from	AuditLog;

-- Security
delete from	Login;
delete from	Role;

-- Meta Data
delete from	MetaProperty;

/*
------------------------------------------------------------------------------
M A S T E R   D A T A
------------------------------------------------------------------------------
*/

-- MetaProperty
insert into MetaProperty (OID, description, valueJSON) values('Trainer.Source'			, 'Trainer Source'				, '["Internal", "External Local", "External Foreign"]');
commit;

-- Role
insert into Role (roleID, roleDescription, menuJSON) values('SA', 'Super Admin', '[
	{ 
		"id" : "dashboard", 
		"url" : "dashboard", 
		"text" : "Dashboard", 
		"class" : "fa fa-dashboard", 
		"child" : [] 
	},{ 
		"id" : "security", 
		"url" : "users", 
		"text" : "Security", 
		"class" : "fa fa-sun-o", 
		"child" : [
			{
				"id" : "user", 
				"url" : "user", 
				"text" : "New User", 
				"class" : "fa fa-user"
			},
			{
				"id" : "users", 
				"url" : "users", 
				"text" : "Users", 
				"class" : "fa fa-users"
			},
			{
				"id" : "changepassword", 
				"url" : "changepassword", 
				"text" : "Change Password", 
				"class" : "fa fa-key"
			}
		] 
	},
	{ 
		"id" : "admission", 
		"url" : "admission", 
		"text" : "Admission", 
		"class" : "fa fa-sun-o", 
		"child" : [
			{
				"id" : "admissionform", 
				"url" : "admissionform", 
				"text" : "Admission Apply Form", 
				"class" : "fa fa-user"
			},
			{
				"id" : "checkeligible", 
				"url" : "checkeligible", 
				"text" : "Varify Student", 
				"class" : "fa fa-users"
			}
		] 
	}

]');

-- Login
insert into Login (loginID,	roleID, password, status, name, imagePath, createdOn, createdBy) Values ('ktanim', 'SA', 'secl2015', 'A', 'Md. Kamruzzaman Tanim', 'images/user/Tanim.jpg', 'GetDate()', 'ktanim');
insert into Login (loginID,	roleID, password, status, name, imagePath, createdOn, createdBy) Values ('monjur', 'SA', 'secl2015', 'A', 'Monjur Ahmed', 'images/user/MonjurAhmed.jpg', 'GetDate()','monjur');
insert into Login (loginID,	roleID, password, status, name, imagePath, createdOn, createdBy) Values ('tojib', 'SA', 'secl2015', 'A', 'Tojibul Islam', 'images/user/TojibulIslam.jpg', 'GetDate()','tojib');
insert into Login (loginID,	roleID, password, status, name, imagePath, createdOn, createdBy) Values ('sharafat', 'SA', 'secl2015', 'I', 'Md. Sharafat Hossain', 'images/user/Tanim.jpg', 'GetDate()', 'ktanim');
commit;



