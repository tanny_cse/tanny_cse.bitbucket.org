
-- connect as oppofinancedb and insert the following data into tables

-- NOTE: Oracle	TO_DATE('2012-03-01 17:51:23', 'YYYY-MM-DD HH24:MI:SS')
-- NOTE: MySQL	STR_TO_DATE('2012-03-01 17:51:23', '%Y-%m-%d %H:%i%s')

use rhddpp;

/*
------------------------------------------------------------------------------
Delete script
------------------------------------------------------------------------------
*/

DELETE FROM	LoginLog;

-- Security, Setup and Meta Data
DELETE FROM	Login;
DELETE FROM	Role;
DELETE FROM	MetaProperty;


commit;


/*
------------------------------------------------------------------------------
M A S T E R   D A T A
------------------------------------------------------------------------------
*/

-- Role
insert into Role (OID, roleDescription, menuJSON) values('SA_OID', 'Super Admin', '[
	{
        "id": "saDashboard",
        "url": "sadashboard",
        "text": "Dashboard",
        "class": "fa fa-dashboard"
    },
	{
		"id" : "manageUser", 
		"url" : "manageuser", 
		"text" : "Manage User", 
		"class" : "fa fa-user"
	},
	{
        "id": "security",
        "url": "",
        "text": "Security",
        "class": "fa fa-sun-o",
        "child": [
			{
				"id" : "changePassword", 
				"url" : "changepassword", 
				"text" : "Change Password", 
				"class" : "fa fa-user"
			},
            {
                "id": "signout",
                "url": "signin",
                "text": "Sign Out",
                "class": "fa fa-user"
            }
        ]
    }
]');

insert into Role (OID, roleDescription, menuJSON) values('Admin_OID', 'Admin', '[
	{ 
		"id" : "adminDashboard", 
		"url" : "admindashboard", 
		"text" : "Dashboard", 
		"class" : "fa fa-dashboard", 
		"child" : [] 
	},
	{ 
		"id" : "systemAdmin", 
		"url" : "", 
		"text" : "System Admin", 
		"class" : "fa fa-sun-o", 
		"child" : [
			{
				"id" : "setupData",	
				"url" : "setupdata", 
				"text" : "Setup Data", 
				"class" : "fa fa-user"
			}
		] 
	},	
	{
        "id": "report",
        "url": "",
        "text": "Report",
        "class": "fa fa-sun-o",
        "child": [
			{
                "id": "ofWallet",
                "url": "mywallet",
                "text": "OF Wallet",
                "class": "fa fa-user"
            },			
			{
                "id": "memberReport",
                "url": "memberreport",
                "text": "List of Members",
                "class": "fa fa-user"
            }
        ]
    },
	{
        "id": "security",
        "url": "",
        "text": "Security",
        "class": "fa fa-sun-o",
        "child": [
			{
				"id" : "changePassword", 
				"url" : "changepassword", 
				"text" : "Change Password", 
				"class" : "fa fa-user"
			},
			{
                "id": "userProfile",
                "url": "userprofile",
                "text": "My Profile",
                "class": "fa fa-user"
            },
            {
                "id": "signout",
                "url": "signin",
                "text": "Sign Out",
                "class": "fa fa-user"
            }
        ]
    }
]');

insert into Role (OID, roleDescription, menuJSON) values('USER_OID', 'User', '[
    {
        "id": "userDashboard",
        "url": "userdashboard",
        "text": "Dashboard",
        "class": "fa fa-dashboard",
        "child": []
    },
    {
        "id": "Dpp",
        "url": "",
        "text": "DppEntry",
        "class": "fa fa-sun-o",
        "child": [
            {
                "id": "newDppEntry",
                "url": "newdppentry",
                "text": "Dpp Entry Info",
                "class": "fa fa-user"
            },
			{
                "id": "dppInfoList",
                "url": "dppinfolist",
                "text": "DppInfo List",
                "class": "fa fa-user"
            }
        ]
    },
    {
        "id": "myAccount",
        "url": "",
        "text": "My Account",
        "class": "fa fa-sun-o",
        "child": [
            {
                "id": "myWallet",
                "url": "mywallet",
                "text": "My Wallet",
                "class": "fa fa-user"
            }
        ]
    },
    {
        "id": "report",
        "url": "",
        "text": "Report",
        "class": "fa fa-sun-o",
        "child": [
            {
                "id": "transactionReport",
                "url": "",
                "text": "Transaction Report",
                "class": "fa fa-sun-o",
                "child": [
                    {
                        "id": "withdrawalReport",
                        "url": "withdrawalreport",
                        "text": "List of Withdrawals",
                        "class": "fa fa-user"
                    }
                ]
            }
		]
    },
    {
        "id": "security",
        "url": "",
        "text": "Security",
        "class": "fa fa-sun-o",
        "child": [
			{
				"id" : "changePassword", 
				"url" : "changepassword", 
				"text" : "Change Password", 
				"class" : "fa fa-user"
			},
			{
                "id": "memberProfile",
                "url": "memberprofile",
                "text": "My Profile",
                "class": "fa fa-user"
            },
            {
                "id": "signout",
                "url": "signin",
                "text": "Sign Out",
                "class": "fa fa-user"
            }
        ]
    }
]');

-- Login
insert into Login (OID,	loginID, userPassword, roleOID, firstName) Values ('oid1',	'sarwar',	'1234', 'USER_OID', 'Sarwar Murshid');
insert into Login (OID,	loginID, userPassword, roleOID, firstName) Values ('oid2', 	'kawsar',	'1234', 'Admin_OID', 'Kawsar Jahan');


